docker build --tag neo4j .
docker run -d --name neo4j -p 7474:7474 -p 7473:7473 -p 7687:7687 neo4j
echo "Waiting for container creation"
sleep 5
docker stop neo4j
sleep 1
docker cp graph.db neo4j:/var/lib/neo4j/data/databases
docker start neo4j
docker logs -f neo4j