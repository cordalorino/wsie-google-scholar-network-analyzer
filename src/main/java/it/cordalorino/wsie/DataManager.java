package it.cordalorino.wsie;

import it.cordalorino.wsie.constants.Addresses;
import it.cordalorino.wsie.constants.FilesNames;
import it.cordalorino.wsie.scraper.*;
import it.cordalorino.wsie.domain.*;
import it.cordalorino.wsie.service.AuthorService;
import it.cordalorino.wsie.service.ClusterService;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Starts the download of Google Scholar data and uploads them in Neo4j
 */
public class DataManager {

    private HashMap<String, Author> authorMap;
    private ArrayList<Cluster> clusterArrayList;

    private boolean fileChargedInMemory = false;

    static Logger logger;

    static {
        logger = Logger.getLogger(DataManager.class);
    }

    private String mostCitedId;


    /**
     * Download or read from cache all the data,
     * then fills the missing data from the coauthor with the information from the authors.
     * Finds the cluster with most citations and downloads data from this articles.
     */
    public void download() {

        logger.info("Step 1) Venues Data");

        ArrayList<Venue> venueArrayList = getVenuesList();

        logger.info("Step 2) Articles Data");

        ArrayList<Article> articleArrayList = getArticlesList(venueArrayList);

        logger.info("Step 3) Clusters Data");

        clusterArrayList = getClustersList(articleArrayList);

        logger.info("Step 4) Author Scraper");

        authorMap = getAuthorMap(clusterArrayList);

        fillAuthor(clusterArrayList, authorMap);

        logger.info("Step 5) Find Most Popular");

        Cluster mostPopularCluster = getMostPopularCluster(clusterArrayList);
        mostCitedId = mostPopularCluster.getEntityId();
        logger.info("Step 6) Citation Scraper");

        Set<Cluster> citationPageArrayList = getClusterCitation(mostPopularCluster);

        fillAuthor(citationPageArrayList, authorMap);

        mostPopularCluster.setCited_by(citationPageArrayList);

        fileChargedInMemory = true;

        logger.info("Step 6) Citation Scraper finished");

    }

    /**
     * Uploads Authors and Clusters data into the database using services
     */
    public void uploadToDB() {

        AuthorService authorService = new AuthorService();
        ClusterService clusterService = new ClusterService();

        int index = 1;
        int listSize = authorMap.size();

        authorService.beginTransaction();

        for (Author a : authorMap.values()) {
            authorService.createOrUpdate(a);
            logger.info(String.format("Adding Authors to db %d of %d", index, listSize));
            index++;
        }
        authorService.commit();

        index = 1;
        listSize = clusterArrayList.size();

        clusterService.beginTransaction();

        for (Cluster c : clusterArrayList) {
            clusterService.createOrUpdate(c);
            logger.info(String.format("Adding Clusters/Articles to db %d of %d", index, listSize));
            index++;
        }

        clusterService.commit();
    }


    /**
     * Fills the missing information in each cluster author's list if missing
     * @param clusterCollection All clusters downloaded
     * @param authorMap All authors downloaded
     */

    private void fillAuthor(Collection<Cluster> clusterCollection, HashMap<String, Author> authorMap) {

        for (Cluster c : clusterCollection) {

            Set<Author> updatedClusterSet = new HashSet<>();

            for (Author author : c.getAuthors()) {

                Author fullAuthor = authorMap.get(author.getEntityId());

                if (fullAuthor != null) {
                    updatedClusterSet.add(fullAuthor);
                } else {
                    updatedClusterSet.add(author);
                }

            }

            c.setAuthors(updatedClusterSet);
        }
    }


    /**
     * Looks for the cluster with most citations
     * @param clusterArrayList All the clusters
     * @return The cluster with most citations
     */
    private Cluster getMostPopularCluster(ArrayList<Cluster> clusterArrayList) {
        Cluster mostPopularCluster;
        mostPopularCluster = clusterArrayList.get(0);

        for (Cluster c : clusterArrayList) {
            if (c.getCited() > mostPopularCluster.getCited()) {
                mostPopularCluster = c;
            }
        }
        return mostPopularCluster;
    }


    private Set<Cluster> getClusterCitation(Cluster mostPopularCluster) {

        MostPopularClusterScraper mostPopularClusterScraper = new MostPopularClusterScraper();
        String clusterId = mostPopularCluster.getEntityId();
        String pageUrl = Addresses.MOST_CITED_CLUSTER_CITATIONS(clusterId);

        int pageNum = 0;
        Set<Cluster> citationPageArrayList = new HashSet<>();
        while (true) {
            logger.info((pageNum + 1) + ") Downloading citation page");

            CitationPage citationPage = mostPopularClusterScraper.scrap(
                    pageUrl + "&start=" + (10 * pageNum),
                    FilesNames.CITATION_PAGE(clusterId, pageNum));
            pageNum++;

            citationPageArrayList.addAll(citationPage.getClusters());

            if (citationPage.getClusters().size() < 10) {
                break;
            }

        }

        return citationPageArrayList;
    }

    private HashMap<String, Author> getAuthorMap(ArrayList<Cluster> clusterArrayList) {

//        ArrayList<Author> authorArrayList = new ArrayList<>();

        HashMap<String, Author> authorHashMap = new HashMap<>();


        AuthorScraper authorScraper = new AuthorScraper();

        for (int i = 0; i < clusterArrayList.size(); i++) {
            Cluster cluster = clusterArrayList.get(i);
            logger.info(i + ") " + cluster.getTitle());

            Set<Author> authors = cluster.getAuthors();
            int j = 0;
            for (Author author : authors) {
                String userId = author.getEntityId();

                Author tempAuthor = authorScraper.scrap(
                        Addresses.USER_PAGE + userId,
                        FilesNames.AUTHOR(userId));

                authorHashMap.put(userId, tempAuthor);
                j++;
            }

        }
        return authorHashMap;
    }

    private ArrayList<Cluster> getClustersList(ArrayList<Article> articleArrayList) {
        ClusterScraper clusterScraper = new ClusterScraper();
        ArrayList<Cluster> clusterArrayList = new ArrayList<>();
        for (int i = 0; i < articleArrayList.size(); i++) {

            Article article = articleArrayList.get(i);
            String clusterId = article.getClusterId();
            logger.info(i + ") " + article.getTitle() + " " + clusterId);

            Cluster clusterTemp = clusterScraper.scrap(Addresses.CLUSTER_PAGE(clusterId), FilesNames.CLUSTER(clusterId));
            clusterArrayList.add(clusterTemp);

        }
        return clusterArrayList;
    }

    private ArrayList<Article> getArticlesList(ArrayList<Venue> venueArrayList) {
        ArrayList<Article> articleArrayList = new ArrayList<>();
        ArrayList<Article> articlesPartial;
        ArticlesScraper articlesScraper = new ArticlesScraper();

        for (int i = 0; i < venueArrayList.size(); i++) {

            Venue venue = venueArrayList.get(i);
            logger.info(i + ") " + venue.getTitle());

            for (int j = 0; j < 5; j++) {

                String articlesPage = Addresses.VENUE_ARTICLE_LIST(venue.getLink(), j);
                String articleFilename = FilesNames.ARTICLES_LIST(i, j);

                logger.info("Fetching article list " + (j + 1) + " of " + "5");

                articlesPartial = articlesScraper.scrap(articlesPage, articleFilename);
                articleArrayList.addAll(articlesPartial);
            }
        }
        return articleArrayList;
    }

    private ArrayList<Venue> getVenuesList() {
        ArrayList<Venue> venueArrayList = null;
        VenuesScraper venuesScraper;
        venuesScraper = new VenuesScraper();
        venueArrayList = venuesScraper.scrap(Addresses.AI_VENUES, FilesNames.VENUES_LIST);
        return venueArrayList;
    }

    public boolean isFileLoadedInMemory() {
        return fileChargedInMemory;
    }

    public void configureLogger(Appender textArea1) {
        Logger.getRootLogger().addAppender(textArea1);
    }

    public ArrayList<Cluster> getClusters() {
        return clusterArrayList;
    }

    public String getMostCitedId() {
        return mostCitedId;
    }
}
