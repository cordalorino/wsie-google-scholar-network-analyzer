package it.cordalorino.wsie.domain;


import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.Set;

/**
 * Extends DomainEntity. Maps the Author information to the Neo4j database
 */
@NodeEntity
public class Author extends DomainEntity {

    @Index(unique = true, primary = true)
    private String entityId;

    private String name;

    @Relationship(type = "COAUTHOR", direction = Relationship.INCOMING)
    private Set<Author> coauthors;

    @Relationship(type = "KEYWORD", direction = Relationship.OUTGOING)
    private Set<Keyword> keywords;

    public Author() {
    }

    public Author(String entityId, String name) {
        this.entityId = entityId;
        this.name = name;
    }

    public Author(String stringId, String name, Set<Author> coauthors, Set<Keyword> keywords) {
        this.entityId = entityId;
        this.name = name;
        this.coauthors = coauthors;
        this.keywords = keywords;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Author> getCoauthors() {
        return coauthors;
    }

    public void setCoauthors(Set<Author> coauthors) {
        this.coauthors = coauthors;
    }

    public Set<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<Keyword> keywords) {
        this.keywords = keywords;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    @Override
    public String toString() {
        return "{name='" + name + '\'' +
                ", entityId='" + entityId + '\'' +
                '}';
    }


}
