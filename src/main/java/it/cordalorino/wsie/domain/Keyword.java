package it.cordalorino.wsie.domain;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;



/**
 * Extends DomainEntity. Maps the Keyword user by authors to the Neo4j database
 */
@NodeEntity
public class Keyword extends DomainEntity {

    @Index(unique = true, primary = true)
    private String entityId;

    private String keyword;

    public Keyword() {
    }

    public Keyword(String entityId, String keyword) {
        this.entityId = entityId;
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
