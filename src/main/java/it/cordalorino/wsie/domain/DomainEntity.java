package it.cordalorino.wsie.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;

/**
 * Provides the basic mapping needed import data in Neo4j.
 * All entity added to Neo4j extends this class.
 * */
public abstract class DomainEntity {

    @GraphId
    protected Long id;


    public DomainEntity() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public abstract String getEntityId();

}
