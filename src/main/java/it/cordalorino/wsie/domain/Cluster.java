package it.cordalorino.wsie.domain;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Extends DomainEntity. Maps the Cluster information to the Neo4j database.
 * Cluster in Google School represents all the version of the same article
 */
@NodeEntity(label = "Article")
public class Cluster extends DomainEntity{

    @Index(unique = true, primary = true)
    private String entityId;

    private String title;
    private int cited;
    private String venueSite;
    private String link;
    private String textAbstract;
    private int year;

    @Relationship(type = "AUTHOR", direction = Relationship.INCOMING)
    private Set<Author> authors = new HashSet<>();

    @Relationship(type = "CITED_BY", direction = Relationship.OUTGOING)
    private Set<Cluster> cited_by = new HashSet<>();

    public Cluster() {
    }

    public Cluster(String entityId) {
        this.entityId = entityId;
    }

    public Cluster(String entityId, String title, int cited,
                   String venueSite, String link, String textAbstract,
                   int year) {
        this.entityId = entityId;
        this.title = title;
        this.cited = cited;
        this.venueSite = venueSite;
        this.link = link;
        this.textAbstract = textAbstract;
        this.year = year;
    }

    public Cluster(String entityId, String title, int cited,
                   String venueSite, String link, String textAbstract,
                   int year, Set<Author> authors) {
        this.entityId = entityId;
        this.title = title;
        this.cited = cited;
        this.venueSite = venueSite;
        this.link = link;
        this.textAbstract = textAbstract;
        this.year = year;
        this.authors = authors;
    }

    public Cluster(String entityId, String title, int cited,
                   String venueSite, String link, String textAbstract,
                   int year, Set<Author> authors, Set<Cluster> cited_by) {
        this.entityId = entityId;
        this.title = title;
        this.cited = cited;
        this.venueSite = venueSite;
        this.link = link;
        this.textAbstract = textAbstract;
        this.year = year;
        this.authors = authors;
        this.cited_by = cited_by;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCited() {
        return cited;
    }

    public void setCited(int cited) {
        this.cited = cited;
    }

    public String getVenueSite() {
        return venueSite;
    }

    public void setVenueSite(String venueSite) {
        this.venueSite = venueSite;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTextAbstract() {
        return textAbstract;
    }

    public void setTextAbstract(String textAbstract) {
        this.textAbstract = textAbstract;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Set<Cluster> getCited_by() {
        return cited_by;
    }

    public void setCited_by(Set<Cluster> cited_by) {
        this.cited_by = cited_by;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
