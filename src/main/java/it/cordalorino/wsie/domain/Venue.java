package it.cordalorino.wsie.domain;

/**
 * Don't implements DomainEntity like other classes in this package.
 * Used only for serializing data with GSON, then saved in cache.
 */
public class Venue {

    private int elementIndex;
    private final String title;
    private final String link;
    private int h5_index,h5_median;

    public Venue(String title, String link, int h5_index, int h5_median, int elementIndex) {

        this.title = title;
        this.link = link;
        this.h5_index = h5_index;
        this.h5_median = h5_median;
        this.elementIndex = elementIndex;
    }

    @Override
    public String toString() {
        return "Venue{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", h5_index=" + h5_index +
                ", h5_median=" + h5_median +
                ", elementIndex=" + elementIndex +
                '}';
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }
}
