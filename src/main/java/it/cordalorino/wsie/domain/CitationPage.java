package it.cordalorino.wsie.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;

import java.util.ArrayList;
import java.util.Set;


/**
 * Don't implements DomainEntity like other classes in this package.
 * Used only for serializing data with GSON, then saved in cache.
 */
public class CitationPage {

    private Cluster cited;
    private Set<Cluster> clusters;

    public CitationPage() {
    }

    public CitationPage(Cluster cited, Set<Cluster> clusters) {
        this.cited = cited;
        this.clusters = clusters;
    }

    public Cluster getCited() {
        return cited;
    }

    public void setCited(Cluster cited) {
        this.cited = cited;
    }

    public Set<Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(Set<Cluster> clusters) {
        this.clusters = clusters;
    }
}
