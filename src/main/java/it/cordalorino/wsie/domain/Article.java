package it.cordalorino.wsie.domain;

/**
 * Don't implements DomainEntity like other classes in this package.
 * Used only for serializing data with GSON, then saved in cache.
 */
public class Article {
    String title;
    String clusterId;
    String year;
    String citedByUrl;

    public Article(String title, String clusterId, String year, String citedByUrl) {
        this.title = title;
        this.clusterId = clusterId;
        this.year = year;
        this.citedByUrl = citedByUrl;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + title + '\'' +
                ", clusterId='" + clusterId + '\'' +
                ", year='" + year + '\'' +
                ", citedByUrl='" + citedByUrl + '\'' +
                '}';
    }

    public String getClusterId() {
        return clusterId;
    }
}
