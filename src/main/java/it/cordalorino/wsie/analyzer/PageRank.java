package it.cordalorino.wsie.analyzer;

import org.neo4j.ogm.model.Result;

import java.util.Collections;

/**
 * Created by andre on 18/07/2017.
 */
public class PageRank extends Analyzer {

    private String label;
    private String relationship;
    private int iterations;
    private double dampingFactor;
    private String writeProperty;

    public PageRank(String label, String relationship, int iterations, double dampingFactor, String writeProperty) {

        this.label = label;
        this.relationship = relationship;
        this.iterations = iterations;
        this.dampingFactor = dampingFactor;
        this.writeProperty = writeProperty;
    }

//    @Override
//    public void compute() {
//        String dampingFactorString = String.format("%.2f", dampingFactor).replace(",", ".");
//
//        String query = String.format(
//                "CALL algo.pageRank('%s', '%s', {iterations:%d, dampingFactor:%s,\n" +
//                        "write: true,writeProperty:\"pagerank\"})\n" +
//                        "YIELD nodes, iterations, loadMillis, computeMillis, " +
//                        "writeMillis, dampingFactor, write, writeProperty",
//                label, relationship, iterations, dampingFactorString, writeProperty
//        );
//
//
//        logger.info("Starting computation for " + this.getClass().getSimpleName());
//
//        session.query(query, Collections.emptyMap());
//
//        logger.info("Done!");
//    }
//
//    @Override
//    public void queryAndDisplay(Order order, int limit) {
//
//        String query = String.format(
//                "MATCH (n:%s) RETURN n as %s, n.%s as Pagerank_Score ORDER BY n.%s %s LIMIT %d",
//                label, label, writeProperty, writeProperty, order.get(), limit
//        );
//
//        Result r = session.query(query, Collections.emptyMap());
//
//        printAndReturnResult(r);
//    }

    @Override
    String getComputeQuery() {

        String dampingFactorString = String.format("%.2f", dampingFactor).replace(",", ".");

        return String.format(
                "CALL algo.pageRank('%s', '%s', {iterations:%d, dampingFactor:%s,\n" +
                        "write: true,writeProperty:\"pagerank\"})\n" +
                        "YIELD nodes, iterations, loadMillis, computeMillis, " +
                        "writeMillis, dampingFactor, write, writeProperty",
                label, relationship, iterations, dampingFactorString, writeProperty
        );
    }

    @Override
    String getDisplayQuery(Order order, int limit) {
        return String.format(
                "MATCH (n:%s) RETURN n as %s, n.%s as Pagerank_Score ORDER BY n.%s %s LIMIT %d",
                label, label, writeProperty, writeProperty, order.get(), limit
        );
    }



}
