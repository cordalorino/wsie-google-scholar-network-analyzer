package it.cordalorino.wsie.analyzer;

import org.neo4j.ogm.model.Result;

import java.util.Collections;

/**
 * Created by andre on 18/07/2017.
 */
public class Betweenness extends Analyzer {


    private String label;
    private String relationship;
    private String betweennessProperty;

    public Betweenness(String label, String relationship, String betweennessProperty) {
        this.label = label;
        this.relationship = relationship;
        this.betweennessProperty = betweennessProperty;
    }


//    @Override
//    public void compute() {
//        String query = String.format(
//                "CALL algo.betweenness('%s', '%s', {write:true, writeProperty:'%s'})\n" +
//                        "YIELD nodes, minCentrality, maxCentrality, sumCentrality, loadMillis, computeMillis, writeMillis",
//                label, relationship, betweennessProperty
//        );
//
//        logger.info("Starting computation for " + this.getClass().getSimpleName());
//
//        setStartTimer();
//
//        session.query(query, Collections.emptyMap());
//
//        logger.info("Done in " + getComputationTimer() + "m");
//    }
//
//    @Override
//    public void queryAndDisplay(Order order, int limit) {
//
//        String query = String.format(
//                "MATCH (n:%s) RETURN n as %s , n.%s as Betweenness ORDER BY n.%s %s LIMIT %d",
//                label, label, betweennessProperty, betweennessProperty, order.get(), limit
//        );
//
//
//        Result r = session.query(query, Collections.emptyMap());
//
//        printAndReturnResult(r);
//    }

    @Override
    String getComputeQuery() {
        return String.format(
                "CALL algo.betweenness('%s', '%s', {write:true, writeProperty:'%s'})\n" +
                        "YIELD nodes, minCentrality, maxCentrality, sumCentrality, loadMillis, computeMillis, writeMillis",
                label, relationship, betweennessProperty
        );
    }

    @Override
    String getDisplayQuery(Order order, int limit) {
        return String.format(
                "MATCH (n:%s) RETURN n as %s , n.%s as Betweenness ORDER BY n.%s %s LIMIT %d",
                label, label, betweennessProperty, betweennessProperty, order.get(), limit
        );
    }



}
