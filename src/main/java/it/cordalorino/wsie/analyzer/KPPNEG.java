package it.cordalorino.wsie.analyzer;

import it.cordalorino.wsie.database.Neo4jSessionFactory;
import org.apache.commons.lang3.StringUtils;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;


import java.util.*;

/**
 * Created by andre on 18/07/2017.
 */
public class KPPNEG extends Analyzer {

    // private Session session;

    private String label = "Author";
    private String temporaryLabel = "Authorkpp";
    private String relationship = "COAUTHOR";
    private String writeProperty = "kppneg";


    public KPPNEG(String label, String relationship, String writeProperty) {

        //  this.session = Neo4jSessionFactory.getInstance().getNeo4jSession();

        this.label = label;
        this.relationship = relationship;
        this.writeProperty = writeProperty;
        this.temporaryLabel = label + "Temp" + getRandom();

    }


    private int getRandom() {
        return (int) (Math.random() * 11);
    }

    @Override
    public void compute() {

        ArrayList<Integer> ids = getIDs();

        Map<Integer, Integer> scores = new HashMap<>();

        int score;
        int originalReachability = computeReachability();
        String output;

        logger.info("Starting computation for " + this.getClass().getSimpleName());

        setStartTimer();

        Transaction transaction = session.beginTransaction();

        int i = 1, size = ids.size();

        System.out.println("");

        for (Integer id : ids) {

            changeLabel(id, label, temporaryLabel);

            score = Math.abs(originalReachability - computeReachability());

            changeLabel(id, temporaryLabel, label);

            setScore(id, score);

            output = String.format("NodeId: %d of %d - Score: %d", i, size, score);

            logger.info(output);

            i++;
        }

        transaction.commit();

        transaction.close();

        logger.info("Done in " + getComputationTimer());

    }
    
    @Override
    String getComputeQuery() {
        return "";
    }

    @Override
    String getDisplayQuery(Order order, int limit) {
        return String.format(
                "MATCH (n:%s) RETURN n as %s, n.%s as KPPNEG_Score ORDER BY n.%s %s LIMIT %d",
                label, label, writeProperty, writeProperty, order.get(), limit
        );
    }


    private ArrayList<Integer> getIDs() {

        String query = String.format("MATCH (n:%s) RETURN id(n)", label);

        Result result = session.query(query, Collections.emptyMap());

        ArrayList<Integer> ids = new ArrayList<>();

        for (Map<String, Object> m : result.queryResults()) {

            int id = (int) m.get("id(n)");
            ids.add(id);
        }

        return ids;
    }

    private void changeLabel(int id, String from, String to) {

        String query = String.format(
                "START b = node(%d)\n" +
                        "REMOVE b:%s\n" +
                        "SET b:%s", id, from, to);

        session.query(query, Collections.emptyMap());
    }

    private void setScore(int id, int score) {

        String query = String.format(
                "START b = node(%d)\n" +
                        "SET b.%s = %d", id, writeProperty, score);

        session.query(query, Collections.emptyMap());
    }

    /**
     * Done with unionfind
     */
    private int computeReachability() {

//        String query = String.format(
//                "CALL algo.unionFind.stream('%s', '%s', {})\n" +
//                        "YIELD nodeId,setId\n" +
//                        "WITH distinct(setId) as c, count(*) as size \n" +
//                        "RETURN sum(size) as sum_size", label, relationship
//        );


        String query = String.format(
                "CALL algo.unionFind.stream('%s', '%s', {})\n" +
                        "YIELD nodeId,setId\n" +
                        "RETURN distinct(setId) as c, count(*) as size \n",
                label, relationship
        );


        Result result = session.query(query, Collections.emptyMap());

        int reachability = 0;

        int clusterSize;

        for (Map<String, Object> m : result.queryResults()) {

            clusterSize = (int) m.get("size");

            if (clusterSize == 1) {
                reachability += 1;
            } else {
                reachability += clusterSize * (clusterSize - 1);
            }

        }

        return reachability;

    }


}
