package it.cordalorino.wsie.analyzer.lucene;

import it.cordalorino.wsie.analyzer.lucene.math.CosineSimilarity;
import it.cordalorino.wsie.analyzer.lucene.math.TermVector;
import it.cordalorino.wsie.analyzer.lucene.model.ArticleAbstract;
import it.cordalorino.wsie.analyzer.lucene.utils.MapUtil;
import it.cordalorino.wsie.domain.Cluster;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import java.io.IOException;
import java.util.*;

import static org.apache.lucene.util.Version.LUCENE_41;

public class LuceneAnalyzer {

    private final String mostCitedEntityId;
    private IndexWriter writer;
    private Directory dir;
    private IndexSearcher searcher;
    private IndexReader indexReader;

    private boolean hasTfIdfAlreadyBeenCalculated = false;
    static Logger logger;


    static {
        logger = Logger.getLogger(it.cordalorino.wsie.analyzer.Analyzer.class);
    }

    private HashMap<String, Double> hashMapSimilarities;

    public LuceneAnalyzer(ArrayList<Cluster> clusters, String mostCitedEntityId) {
        setupLucene();
        this.mostCitedEntityId = mostCitedEntityId;
        loadArticlesOnMemory(clusters);
        setUpIndexReader();
    }

    public Map<String, Float> analyzeTFIDF(){
        TfIdfCalculator tf_idfCalculator = new TfIdfCalculator();
        try {
            TermVector[] textAbstracts = tf_idfCalculator.scoreCalculator(indexReader, indexReader.maxDoc(), "textAbstract");

            int idMostCited = findMostCitedId();
            calcSimilaritiesMap(textAbstracts, idMostCited);
            hasTfIdfAlreadyBeenCalculated = true;
        } catch (IOException e) {
            e.printStackTrace();
        };
        return null;
    }

    private void calcSimilaritiesMap(TermVector[] textAbstracts, int idMostCited) throws IOException {
        hashMapSimilarities = new HashMap<>();
        for (int i = 0; i < textAbstracts.length; i++) {
            if(i == idMostCited)
                continue;
            double cosineSimilarity = CosineSimilarity.CosineSimilarity(textAbstracts[idMostCited], textAbstracts[i]);
            hashMapSimilarities.put(indexReader.document(i).get("entityId"),cosineSimilarity);
        }
        hashMapSimilarities = MapUtil.sortByValue(hashMapSimilarities);
        for (String key: hashMapSimilarities.keySet()
             ) {
            logger.info("Similarity " + key +" = " + hashMapSimilarities.get(key));
        }
    }

    public boolean isHasTfIdfAlreadyBeenCalculated() {
        return hasTfIdfAlreadyBeenCalculated;
    }

    private int findMostCitedId(){
        Query q = new TermQuery( new Term("entityId",mostCitedEntityId));
        try {
            TopDocs top = searcher.search(q,1);
            ScoreDoc[] hits = top.scoreDocs; // get only the scored documents (ScoreDoc is a tuple)
            Document doc=null;
            return hits[0].doc;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void setUpIndexReader(){

        try {
            indexReader = DirectoryReader.open(dir);
            searcher = new IndexSearcher(indexReader);
            searcher.setSimilarity(new DefaultSimilarity());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setupLucene() {
        try {
            dir = new RAMDirectory();

            Analyzer analyzer = new StandardAnalyzer(LUCENE_41);
            IndexWriterConfig cfg = new IndexWriterConfig(LUCENE_41, analyzer);
            writer = new IndexWriter(dir, cfg);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadArticlesOnMemory(ArrayList<Cluster> clusters){
        for (Cluster c :
                clusters) {
            loadCluster(c);
            Set<Cluster> cited_by = c.getCited_by();
            if(cited_by != null && cited_by.size() > 0){
                for (Cluster cit :
                        cited_by) {
                    loadCluster(cit);
                }
            }
        }
        try {
            writer.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadCluster(Cluster c) {
        String tempAbstract = c.getTextAbstract();
        if (tempAbstract != null){
            Document d = new Document();
            ArticleAbstract articleAbstract = new ArticleAbstract(c.getEntityId(), tempAbstract);
            articleAbstract.setDocument(d);
            try {
                writer.addDocument(d);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getAndPrintFormattedResult(int limit) {
        StringBuilder stringBuilder = new StringBuilder();

        if(hashMapSimilarities != null){
            logger.info("TfIdf score -------- First " +limit + " results");
            stringBuilder.append("TfIdf score -------- First ").append(limit).append(" results").append("\n");
            int count = 1;
            for (String term: hashMapSimilarities.keySet()
                 ) {
                if(count > limit)
                    return null;
                logger.info("-tfidf index " + count +" term: " + term + " score: " + hashMapSimilarities.get(term));
                stringBuilder.append("Index ")
                        .append(count)
                        .append(" term: ")
                        .append(term)
                        .append(" score: ")
                        .append(hashMapSimilarities.get(term)).append("\n");
                count++;
            }
        }

        return stringBuilder.toString();

    }
}
