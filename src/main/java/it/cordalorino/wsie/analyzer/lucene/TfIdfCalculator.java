package it.cordalorino.wsie.analyzer.lucene;

import it.cordalorino.wsie.analyzer.lucene.math.TermVector;
import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.Bits;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class calculates the TfIdf for each term and for each document
 */
public class TfIdfCalculator
{
    static float tf = 1;
    static float idf = 0;
    private float tfidf_score;

    /**
     * Creates an array of TermVector, one for each document.
     * @param reader
     * @param maxDoc
     * @param field
     * @return
     * @throws IOException
     */
    public TermVector[] scoreCalculator(IndexReader reader, int maxDoc, String field) throws IOException
    {

        TFIDFSimilarity tfidfSIM = new DefaultSimilarity();
        Bits liveDocs = MultiFields.getLiveDocs(reader);
        TermsEnum termEnum = MultiFields.getTerms(reader, field).iterator(null);
        // Creates
        TermVector[] termVectors = initializeDocVector(reader, maxDoc, field);

        BytesRef bytesRef;
        while ((bytesRef = termEnum.next()) != null)
        {
             if (termEnum.seekExact(bytesRef, true))
                {
                    //Calculates inverted document frequency for that term
                    idf = tfidfSIM.idf(termEnum.docFreq(), reader.numDocs());
                    DocsEnum docsEnum = termEnum.docs(liveDocs, null);
                    if (docsEnum != null)
                    {
                        int doc;
                        String term = bytesRef.utf8ToString();

                        // for each document calculates the tfidf of the term
                        while((doc = docsEnum.nextDoc())!=DocIdSetIterator.NO_MORE_DOCS)
                        {
                            tf = tfidfSIM.tf(docsEnum.freq());
                            tfidf_score = tf*idf;
                            termVectors[doc].setEntry(term,tfidf_score);
                        }
                    }
                }
            }
        return termVectors;
        }

    private TermVector[] initializeDocVector(IndexReader reader, int maxDoc, String field) throws IOException {
        TermVector[] termVectors = new TermVector[maxDoc];
        Map<String,Integer> allTerms = new HashMap<>();
        int pos = 0;
        for (int docId = 0; docId < maxDoc; docId++) {
            TermsEnum termsEnum = MultiFields.getTerms(reader, field).iterator(null);
            BytesRef text = null;
            while ((text = termsEnum.next()) != null) {
                String term = text.utf8ToString();
                allTerms.put(term, pos++);
            }
        }

        pos = 0;
        for(Map.Entry<String,Integer> s : allTerms.entrySet())
        {
            s.setValue(pos++);
        }

        for (int i = 0; i < termVectors.length; i++) {
            termVectors[i] = new TermVector(allTerms);
        }
        return termVectors;
    }

}