package it.cordalorino.wsie.analyzer.lucene.math;

import java.util.Map;
import org.apache.commons.math.linear.OpenMapRealVector;
import org.apache.commons.math.linear.RealVectorFormat;

/**
 *
 */
public class TermVector {

    public Map terms;
    public OpenMapRealVector vector;

    public TermVector(Map terms) {
        this.terms = terms;
        this.vector = new OpenMapRealVector(terms.size());
    }

    public void setEntry(String term, double freq) {
        if (terms.containsKey(term)) {
            int pos = (int)terms.get(term);
            vector.setEntry(pos, freq);
        }
    }

    public void normalize() {
        double sum = vector.getL1Norm();
        vector = (OpenMapRealVector) vector.mapDivide(sum);
    }

    @Override
    public String toString() {
        RealVectorFormat formatter = new RealVectorFormat();
        return formatter.format(vector);
    }
}