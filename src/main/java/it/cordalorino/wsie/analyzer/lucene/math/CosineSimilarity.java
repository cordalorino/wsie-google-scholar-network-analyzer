package it.cordalorino.wsie.analyzer.lucene.math;


public class CosineSimilarity {
    public static double CosineSimilarity(TermVector d1, TermVector d2) {
        double cosinesimilarity;
        try {
            cosinesimilarity = (d1.vector.dotProduct(d2.vector))
                    / (d1.vector.getNorm() * d2.vector.getNorm());
        } catch (Exception e) {
            return 0.0;
        }
        return cosinesimilarity;
    }
}
