package it.cordalorino.wsie.analyzer.lucene.model;


import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

public class ArticleAbstract {

    StringField entityId;
    TextField textAbstract;

    public ArticleAbstract(String entityId, String textAbstract) {
        this.entityId = new StringField("entityId", entityId, Field.Store.YES);
        this.textAbstract = new TextField("textAbstract", textAbstract, Field.Store.YES);
    }

    public void setDocument(Document document){
        document.add(entityId);
        document.add(textAbstract);
    }
}
