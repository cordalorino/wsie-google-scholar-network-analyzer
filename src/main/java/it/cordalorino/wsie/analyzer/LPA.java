package it.cordalorino.wsie.analyzer;

import org.neo4j.ogm.model.Result;

import java.util.Collections;

/**
 * Created by andre on 18/07/2017.
 */


public class LPA extends Analyzer {

    private String label;
    private String relationship;
    private Direction direction;
    private int iterations;
    private String writeProperty;


    public LPA(String label, String relationship, Direction direction, int iterations, String writeProperty) {
        this.label = label;
        this.relationship = relationship;
        this.direction = direction;
        this.iterations = iterations;
        this.writeProperty = writeProperty;
    }

//    @Override
//    public void compute() {
//        String query = String.format(
//                "CALL algo.labelPropagation('%s', '%s', '%s', " +
//                        "{iterations:%d,partitionProperty:'%s', write:true})\n" +
//                        "YIELD nodes, iterations, loadMillis, computeMillis, " +
//                        "writeMillis, write, partitionProperty",
//                label, relationship, direction.get(), iterations, writeProperty
//        );
//
//
//        logger.info("Starting computation for " + this.getClass().getSimpleName());
//
//        session.query(query, Collections.emptyMap());
//
//        logger.info("Done!");
//    }
//
//    @Override
//    public void queryAndDisplay(Order order, int limit) {
//
//        String query = String.format(
//                "MATCH (c:%s)\n" +
//                        "WITH c.%s AS clusterId, collect(c) AS membersName, count(*) as membersCount\n" +
//                        "where clusterId is not null\n" +
//                        "RETURN clusterId, membersName, membersCount ORDER BY membersCount %s\n" +
//                        "LIMIT %d",
//                label, writeProperty, order.get(), limit
//        );
//
//
//        Result r = session.query(query, Collections.emptyMap());
//
//        printAndReturnResult(r);
//
//    }

    @Override
    String getComputeQuery() {
        return String.format(
                "CALL algo.labelPropagation('%s', '%s', '%s', " +
                        "{iterations:%d,partitionProperty:'%s', write:true})\n" +
                        "YIELD nodes, iterations, loadMillis, computeMillis, " +
                        "writeMillis, write, partitionProperty",
                label, relationship, direction.get(), iterations, writeProperty
        );
    }

    @Override
    String getDisplayQuery(Order order, int limit) {
        return String.format(
                "MATCH (c:%s)\n" +
                        "WITH c.%s AS clusterId, collect(c) AS membersName, count(*) as membersCount\n" +
                        "where clusterId is not null\n" +
//                        "RETURN clusterId, membersName, membersCount ORDER BY membersCount %s\n" +
                        "RETURN clusterId, membersCount ORDER BY membersCount %s\n" +
                        "LIMIT %d",
                label, writeProperty, order.get(), limit
        );
    }




}
