package it.cordalorino.wsie.analyzer;

import it.cordalorino.wsie.database.Neo4jSessionFactory;
import org.apache.commons.lang3.StringUtils;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by andre on 17/07/2017.
 */
public abstract class Analyzer {

    abstract String getComputeQuery();
    abstract String getDisplayQuery(Order order, int limit);

    static Logger logger;

    protected Session session;

    private long startTimer;

    static {
        logger = Logger.getLogger(Analyzer.class);
    }

    public enum Direction {

        IN("INCOMING"),
        OUT("OUTGOING");

        private String direction;

        Direction(String direction) {
            this.direction = direction;
        }

        public String get() {
            return direction;
        }
    }

    public enum Order {

        DESC("DESC"),
        ASC("ASC");

        private String order;

        Order(String order) {
            this.order = order;
        }

        public String get() {
            return order;
        }
    }

    public Analyzer() {

        session = Neo4jSessionFactory.getInstance().getNeo4jSession();
    }

    public void compute() {
        String query = getComputeQuery();

        logger.info("Starting computation for " + this.getClass().getSimpleName());

        setStartTimer();

        session.query(query, Collections.emptyMap());

        logger.info("Done in " + getComputationTimer());
    }

    public String queryAndDisplay(Order order, int limit) {

        String query = getDisplayQuery(order,limit);

        Result r = session.query(query, Collections.emptyMap());

        return printAndReturnResult(r);
    }

    protected void setStartTimer(){
        this.startTimer = System.currentTimeMillis();
    }

    protected String getComputationTimer(){

        long time = (System.currentTimeMillis() - startTimer) / 1000;

        return String.format("%ds", time);
    }

    public String printAndReturnResult(Result result) {

        StringBuilder resultBuilder = new StringBuilder();
        Map<String, Integer> maxColSizeMap = new HashMap<String, Integer>();
        boolean initMaxColSizeMap = true;

        for (Map<String, Object> map : result.queryResults()) {

            for (String key : map.keySet()) {
                if (initMaxColSizeMap) {

                    Object obj = map.get(key);
                    String ColValue;

                    if (obj.getClass() == String[].class) {
                        ColValue = String.join(",", (String[]) obj);
                    } else {
                        ColValue = (obj == null) ? "" : obj.toString();
                    }

                    Integer whoIsBig = Math.max(ColValue.length(), key.length());
                    maxColSizeMap.put(key, whoIsBig);
                } else {
                    String ColValue = (map.get(key) == null) ? "" : map.get(key).toString();
                    Integer whoIsBig = Math.max(ColValue.length(), key.length());
                    whoIsBig = Math.max(maxColSizeMap.get(key), whoIsBig);
                    maxColSizeMap.put(key, whoIsBig);
                }
            }
            initMaxColSizeMap = false;
        }

        // Column HEADER
        for (Map<String, Object> map : result) {
            System.out.println("");
            resultBuilder.append("\n");
            StringBuilder colName = new StringBuilder();
            StringBuilder underLine = new StringBuilder();
            for (String key : map.keySet()) {
                colName.append(StringUtils.rightPad(key, maxColSizeMap.get(key)));
                colName.append(" ");

                underLine.append(StringUtils.repeat('-', maxColSizeMap.get(key)));
                underLine.append(" ");
            }
            // Do one time only
            resultBuilder.append(colName.toString() + "\n");
            System.out.println(colName.toString());
            resultBuilder.append(underLine.toString() + "\n");
            System.out.println(underLine.toString());
            break;
        }

        // Print the rows
        for (Map<String, Object> map : result) {
            StringBuilder row = new StringBuilder();
            for (String key : map.keySet()) {

                Object obj = map.get(key);
                String str;

                if (obj.getClass() == String[].class) {
                    str = String.join(",", (String[]) obj);
                } else {
                    str = obj == null ? "" : obj.toString();
                }

                row.append(StringUtils.rightPad(str, maxColSizeMap.get(key) + 1));
            }
            resultBuilder.append(row +"\n");
            System.out.println(row);
        }

        System.out.println("");
        resultBuilder.append("\n");
        return resultBuilder.toString();

    }
}
