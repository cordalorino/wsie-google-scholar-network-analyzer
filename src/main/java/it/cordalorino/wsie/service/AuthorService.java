package it.cordalorino.wsie.service;

import it.cordalorino.wsie.domain.Author;

/**
 * Service implementation for the class Author.
 */
public class AuthorService extends Service<Author> {

    @Override
    Class<Author> getEntityType() {
        return Author.class;
    }

    @Override
    int getDepth() {
        return 2;
    }

}
