package it.cordalorino.wsie.service;

import it.cordalorino.wsie.domain.Keyword;

/**
 * Service implementation for the class Keyword.
 */
public class KeywordService extends Service<Keyword> {

    @Override
    Class<Keyword> getEntityType() {
        return Keyword.class;
    }

    @Override
    int getDepth() {
        return 0;
    }

}
