package it.cordalorino.wsie.service;

import it.cordalorino.wsie.domain.Cluster;

/**
 * Service implementation for the class Cluster.
 */
public class ClusterService extends Service<Cluster> {

    @Override
    Class<Cluster> getEntityType() {
        return Cluster.class;
    }

    @Override
    int getDepth() {
        return 2;
    }

}
