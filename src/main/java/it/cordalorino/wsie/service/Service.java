package it.cordalorino.wsie.service;

import it.cordalorino.wsie.database.Neo4jSessionFactory;
import it.cordalorino.wsie.domain.DomainEntity;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import java.util.NoSuchElementException;

/**
 * All DomainEntity child classes can use this class to execute CRUD operations on the database.
 * Identifies entities by the entityId not the id
 *
 * @param <T> DomainEntity class
 */

public abstract class Service<T extends DomainEntity> {

    private static final int DEPTH_LIST = 0;
    private static final int DEPTH_ENTITY = 1;
    protected Session session = Neo4jSessionFactory.getInstance().getNeo4jSession();
    protected Transaction transaction;

    public void beginTransaction(){

        if(transaction == null || transaction.status() == Transaction.Status.CLOSED){
            transaction = session.beginTransaction();
        }
    }

    public void commit(){

        if(transaction != null || transaction.status() == Transaction.Status.OPEN){
            transaction.commit();
            transaction.close();
        }
    }
    public void rollback(){

        if(transaction != null || transaction.status() == Transaction.Status.CLOSED){
            transaction.rollback();
            transaction.close();
        }
    }


    public Iterable<T> findAll() {
        return session.loadAll(getEntityType(), DEPTH_LIST);
    }

    public T find(String entityId) {

        try {
            return session.loadAll(getEntityType(), new Filter("entityId", entityId)).iterator().next();
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    public void delete(String entityId) {
        session.delete(this.find(entityId));
    }

    public T createOrUpdate(T entity) {

//        T tempEntity = find(entity.getEntityId());
//
//        if (tempEntity != null) {
//            return tempEntity;
//        }

        session.save(entity, getDepth());
        return find(entity.getEntityId());
    }

    /**
     * All classes that extends this one,
     * need to implement this method to return T class
     * because it cannot be done at run-time otherwise
     *
     * @return Class of T
     */
    abstract Class<T> getEntityType();

    abstract int getDepth();
}