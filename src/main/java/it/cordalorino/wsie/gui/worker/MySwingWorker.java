package it.cordalorino.wsie.gui.worker;

import it.cordalorino.wsie.DataManager;
import it.cordalorino.wsie.analyzer.lucene.LuceneAnalyzer;
import it.cordalorino.wsie.domain.Cluster;
import it.cordalorino.wsie.gui.MainForm;
import it.cordalorino.wsie.analyzer.Analyzer;
import it.cordalorino.wsie.analyzer.Betweenness;
import it.cordalorino.wsie.analyzer.KPPNEG;
import it.cordalorino.wsie.analyzer.LPA;

import javax.swing.*;
import java.util.ArrayList;

public class MySwingWorker extends SwingWorker<String, Object>{

    private Analyzer.Order order = Analyzer.Order.DESC;
    private int limit = 10;

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public enum Action {
        DOWNLOAD,UPLOAD, KPPNEG,COMMUNITY_DETECTION,BETWEENNESS,TFIDF
    }

    private MainForm mainForm;
    DataManager dataManager;
    private Action action;
    private String result = null;
    private LuceneAnalyzer luceneAnalyzer;

    public MySwingWorker(MainForm mainForm, DataManager dataManager, Action action) {
        super();
        this.mainForm = mainForm;
        this.dataManager = dataManager;
        this.action = action;
    }

    public void setOrder(Analyzer.Order order){
        this.order = order;
    }

    public void setLuceneAnalyzer(LuceneAnalyzer luceneAnalyzer) {
        this.luceneAnalyzer = luceneAnalyzer;
    }

    @Override
    protected String doInBackground() throws Exception {
        mainForm.setButtonsEnabled(false);
        switch(action){
            case DOWNLOAD:
                dataManager.download();
                break;
            case UPLOAD:
                dataManager.uploadToDB();
                break;
            case KPPNEG:
                KPPNEG kppneg = new KPPNEG("Author", "COAUTHOR", "kppneg");
                kppneg.compute();
                result =kppneg.queryAndDisplay(order, limit);
                break;
            case COMMUNITY_DETECTION:
                LPA lpa = new LPA("Author", "COAUTHOR", Analyzer.Direction.IN, 20903, "partition");
                lpa.compute();
                result =lpa.queryAndDisplay(order, limit);
                break;
            case BETWEENNESS:
                Betweenness betweenness = new Betweenness("Author", "COAUTHOR", "centrality");
                betweenness.compute();
                result =betweenness.queryAndDisplay(order, limit);
            case TFIDF:
                ArrayList<Cluster> clusters = dataManager.getClusters();
                luceneAnalyzer = new LuceneAnalyzer(clusters,dataManager.getMostCitedId());
                luceneAnalyzer.analyzeTFIDF();
                result = luceneAnalyzer.getAndPrintFormattedResult(limit);
                mainForm.setLuceneAnalyzer(luceneAnalyzer);
                break;

        }
        return null;
    }

    @Override
    protected void done(){
        mainForm.setButtonsEnabled(true);
        if(result!=null)
            mainForm.appendToTextArea(result);
    }
}
