package it.cordalorino.wsie.gui;

import it.cordalorino.wsie.DataManager;
import it.cordalorino.wsie.analyzer.Analyzer;
import it.cordalorino.wsie.analyzer.Betweenness;
import it.cordalorino.wsie.analyzer.KPPNEG;
import it.cordalorino.wsie.analyzer.LPA;
import it.cordalorino.wsie.analyzer.lucene.LuceneAnalyzer;
import it.cordalorino.wsie.gui.worker.MySwingWorker;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

public class MainForm {


    private CustomAppender appender;
    private JPanel panel1;
    private JButton downloadButton;
    private JButton uploadDBButton;
    private JButton computeButton;
    private JComboBox comboBox1;
    private JComboBox comboBox2;
    private JTextArea textArea1;
    private JTextField a10TextField;
    private JButton displayButton;

    static DataManager dataManager = new DataManager();
    private JFrame frame;
    private LuceneAnalyzer luceneAnalyzer;

    static private Logger logger;

    static {
        logger = Logger.getLogger(it.cordalorino.wsie.analyzer.Analyzer.class);
    }

    public MainForm() {
        initializeFrame();
        bindButtonToActions();
        showForm(frame);

        configureLogger();

    }

    private void configureLogger() {
        appender = new CustomAppender(textArea1);
        Logger.getRootLogger().addAppender(appender);
    }

    private void bindButtonToActions() {
        downloadButton.addActionListener(e -> doDownload());
        uploadDBButton.addActionListener(e -> doUploadOnDatabase());
        computeButton.addActionListener(e -> doAnalysis());
        displayButton.addActionListener(e -> displayAnalysisResult());
    }

    private void displayAnalysisResult() {
        clearTextArea();
        switch(comboBox1.getSelectedIndex()){
            case 0:
                KPPNEG kppneg = new KPPNEG("Author", "COAUTHOR", "kppneg");
                textArea1.append(kppneg.queryAndDisplay(getOrder(),getLimit()));
                break;
            case 1:
                LPA lpa = new LPA("Author", "COAUTHOR", Analyzer.Direction.IN, 20903, "partition");
                textArea1.append(lpa.queryAndDisplay(getOrder(),getLimit()));
                break;
            case 2:
                Betweenness betweenness = new Betweenness("Author", "COAUTHOR", "centrality");
                textArea1.append(betweenness.queryAndDisplay(getOrder(),getLimit()));
                break;
            case 3:
                displayTfIdf();
                break;
        }
    }

    private void displayTfIdf() {
        if(luceneAnalyzer != null && luceneAnalyzer.isHasTfIdfAlreadyBeenCalculated())
            textArea1.append(luceneAnalyzer.getAndPrintFormattedResult(getLimit()));
        else
            runAndDisplayTfIdf();
    }

    private void doAnalysis() {
        switch(comboBox1.getSelectedIndex()){
            case 0:
                executeAction(MySwingWorker.Action.KPPNEG);
                break;
            case 1:
                executeAction(MySwingWorker.Action.COMMUNITY_DETECTION);
                break;
            case 2:
                executeAction(MySwingWorker.Action.BETWEENNESS);
                break;
            case 3:
                runAndDisplayTfIdf();
        }
    }

    private void runAndDisplayTfIdf() {
        if(!dataManager.isFileLoadedInMemory()) {
            JOptionPane.showMessageDialog(frame,"Please download first");
            return;
        }
        logger.info("Starting frequency analysis");
        executeAction(MySwingWorker.Action.TFIDF);
    }

    private void clearTextArea() {
        textArea1.setText("");
    }

    private void showForm(JFrame frame) {
        frame.setVisible(true);
    }

    private void initializeFrame() {
        frame = new JFrame("GoogleScholar Analyzer");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
    }

    private void doUploadOnDatabase() {

        if(!dataManager.isFileLoadedInMemory()){
            dataManager.download();
        }
        executeAction(MySwingWorker.Action.UPLOAD);

    }

    private void executeAction(MySwingWorker.Action action) {
        MySwingWorker mySwingWorker = new MySwingWorker(this, dataManager, action);

        switch(action){
            case KPPNEG:
            case COMMUNITY_DETECTION:
            case BETWEENNESS:
                mySwingWorker.setOrder(getOrder());
                mySwingWorker.setLimit(getLimit());
                break;
            case TFIDF:
                mySwingWorker.setLimit(getLimit());
                break;
        }
        mySwingWorker.execute();
    }

    private int getLimit() {
        int limit = 10;
        try{
            limit = Integer.valueOf(a10TextField.getText());
        }catch (NumberFormatException ex){
            a10TextField.setText("10");
        }
        return limit;
    }

    private Analyzer.Order getOrder() {
        switch(comboBox2.getSelectedIndex()){
            case 0:
                return Analyzer.Order.DESC;
            case 1:
                return Analyzer.Order.ASC;
        }
        return null;
    }

    private void doDownload() {
        executeAction(MySwingWorker.Action.DOWNLOAD);
    }

    public void setButtonsEnabled(boolean enabled) {
        downloadButton.setEnabled(enabled);
        uploadDBButton.setEnabled(enabled);
        computeButton.setEnabled(enabled);
        displayButton.setEnabled(enabled);
    }

    public void appendToTextArea(String result) {
        textArea1.append(result);
    }

    public void setLuceneAnalyzer(LuceneAnalyzer luceneAnalyzer) {
        this.luceneAnalyzer = luceneAnalyzer;
    }

    class CustomAppender extends AppenderSkeleton {

        JTextArea textArea;

        public CustomAppender(JTextArea textArea) {
            this.textArea = textArea;
            DefaultCaret caret = (DefaultCaret)textArea.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            textArea.setCaret(caret);
        }

        @Override
        protected void append(LoggingEvent loggingEvent) {
            textArea.append(loggingEvent.getMessage() + "\n");
            textArea.setAutoscrolls(true);
        }

        @Override
        public void close() {

        }

        @Override
        public boolean requiresLayout() {
            return false;
        }
    }
}
