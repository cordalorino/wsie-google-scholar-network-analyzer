package it.cordalorino.wsie.constants;

public class Addresses {

    public static final String SCHOLAR_DOMAIN = "https://scholar.google.com";
    public static final String AI_VENUES = SCHOLAR_DOMAIN +
            "/citations?view_op=top_venues&hl=en&vq=eng_artificialintelligence";
//    public static String MOST_CITED_CLUSTER_CITATIONS = "https://scholar.google.com/scholar?cites=16194105527543080940&as_sdt=2005&sciodt=0,5&hl=en";

    public static final String VENUE_ARTICLE_LIST(String venueUrl, int pageNumber) {
        return Addresses.SCHOLAR_DOMAIN + venueUrl + "&cstart=" + (20 * pageNumber);
    }

    public static final String CLUSTER_PAGE(String clusterId) {
        return Addresses.CLUSTER_SEARCH_ROOT + clusterId;
    }

    public static String MOST_CITED_CLUSTER_CITATIONS (String clusterId){
        return "https://scholar.google.com/scholar?cites=" +
                clusterId + "&as_sdt=2005&sciodt=0,5&hl=en";
    }


    public static final String SEARCH_ROOT = SCHOLAR_DOMAIN + "/scholar?hl=en&q=";

    public static final String CLUSTER_SEARCH_ROOT = SCHOLAR_DOMAIN + "/scholar?hl=en&cluster=";
    public static final String USER_PAGE = SCHOLAR_DOMAIN + "/citations?hl=en&oi=sra&user=";



}
