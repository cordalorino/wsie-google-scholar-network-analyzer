package it.cordalorino.wsie.constants;

import java.io.File;

public class FilesNames {
    private static final String BASE_FOLDER = "dataset";
    private static final String ANALYZER_FOLDER = "analyzer";

    public static final String VENUES_FOLDER = BASE_FOLDER + File.separator + "venue";
    public static final String ARTICLES_FOLDER = BASE_FOLDER + File.separator + "article";
    public static final String CLUSTER_FOLDER = BASE_FOLDER + File.separator + "cluster";
    private static final String AUTHOR_FOLDER = BASE_FOLDER + File.separator + "author";
    private static final String CITATION_PAGE_FOLDER = BASE_FOLDER + File.separator + "citation";
    private static final String KPP_NEG_FOLDER = ANALYZER_FOLDER + File.separator + "kppNeg";

    public static final String VENUES_LIST = VENUES_FOLDER + File.separator + "venueList.json";
    public static final String ARTICLES_LIST = ARTICLES_FOLDER + File.separator + "articleList.json";
    public static String KPP_NEG_FILE = ANALYZER_FOLDER + File.separator + "kppNegBroker.json";;

    public static final String ARTICLES_LIST(int venueIndex, int pageIndex) {
        return ARTICLES_FOLDER +
                File.separator +
                "articleList -v " + venueIndex +
                " -p " + pageIndex +
                ".json";
    }

    public static final String CLUSTER(String clusterId) {
        return CLUSTER_FOLDER +
                File.separator +
                "cluster -id " + clusterId +
                ".json";
    }

    public static final String AUTHOR(String authorId) {
        return AUTHOR_FOLDER +
                File.separator +
                "author -id " + authorId +
                ".json";
    }

    public static final String CITATION_PAGE(String clusterId,int pageNumber) {
        return CITATION_PAGE_FOLDER +
                File.separator +
                "citation -clusterId " + clusterId +
                " -page " + pageNumber +
                ".json";
    }

    public static String KPP_NEG_FILE_FOR_AUTHOR(String name, int i, double value) {
        return KPP_NEG_FOLDER +
                File.separator +
                "broker -index " + i +
                " -name " + name +
                " -brokerValue " + value +
                ".json";
    }
}
