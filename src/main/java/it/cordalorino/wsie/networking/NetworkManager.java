package it.cordalorino.wsie.networking;

import org.apache.log4j.Logger;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Random;

/**
 * Used to route all http requests through Tor.
 * This allowed us to change ip whenever the requests quota to Google Scholar was reached
 */
public class NetworkManager {

    private static int requestCounter = 0;
    private static final int MAX_REQUESTS_PER_IP = 45;

    static Logger logger = Logger.getLogger(NetworkManager.class);


    public static Document executeRequest(String url) throws IOException {
        return executeRequest(url, false);
    }

    /**
     * Download a page via Tor
     *
     * @param url
     * @param ignoreContentType
     * @return
     * @throws IOException
     */
    public static Document executeRequest(String url, boolean ignoreContentType) throws IOException {
        Document document = null;
        if (requestCounter % MAX_REQUESTS_PER_IP == MAX_REQUESTS_PER_IP - 1) {
            restartTorDockerContainer();
            requestCounter = 0;
        }
        try {
            document = Jsoup.connect(url)
                    .proxy("localhost", 8118)
                    .validateTLSCertificates(false).ignoreContentType(ignoreContentType)
                    .userAgent("User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36")
                    .get();
            requestCounter++;
        } catch (HttpStatusException ex) {
            logger.error("HttpStatusException " + ex.getStatusCode());
            restartTorDockerContainer();
            return executeRequest(url);
        } catch (SocketTimeoutException ex) {
            logger.error("SocketTimeoutException ");
            restartTorDockerContainer();
            return executeRequest(url);
        } catch (IOException e) {
            throw e;
        }
        return document;

    }


    public static void renewIp() {
        restartTorDockerContainer();
    }

    /**
     * Restarts the docker container where the Tor proxy is installed
     */
    private static void restartTorDockerContainer() {
        //TODO when restarting tor choose randomly an exit country in order to avoid reusing similar ips
        try {
            logger.info("Renewing IP address");
            Runtime.getRuntime().exec("docker stop tor");
            Thread.sleep(1000);

            Runtime.getRuntime().exec("docker rm tor");
            Thread.sleep(1000);

            String randomCountry = getRandomCountry();
            Runtime.getRuntime().exec("docker run --name tor -it -p 8118:8118 -p 9050:9050 -d dperson/torproxy -l " + randomCountry);
            Thread.sleep(30000);
            logger.info("" + getIp() + " from " + randomCountry);

//            logger.info("Renewing IP address");
//            Runtime.getRuntime().exec("docker exec tor printf \"`head -n -1 torrc`\\n{US}\\n\" > /etc/tor/torrc");
//            Thread.sleep(1000);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String getIp() throws IOException {

        return executeRequest("https://api.ipify.org/?format=json", true).text();
    }

    private static String getRandomCountry() {
        return countries[new Random().nextInt(countries.length)];
    }

    static String[] countries = {
            "PA", "UA", "RO", "NL", "DE", "FR", "LU", "US",
            "HU", "SK", "GB", "CA", "SE", "AT", "DK", "CH",
            "CZ", "PT", "BG", "FI", "NO", "LR", "IT", "RU",
            "SC", "LT", "LV", "ES", "MD", "EU", "EE", "BE",
            "IE", "PL", "TR", "IS", "A1", "KN", "ID", "BR",
            "CL", "JP", "CR", "IN", "GR", "NA", "SG", "IL",
            "PR", "AL", "ZA", "TW", "BZ", "RS", "HR", "AU",
            "HK", "VN", "MX", "IM", "TH", "BY", "AM", "KG",
            "AR", "KR", "MY", "NZ", "AE", "EG", "MN", "SI",
            "PH", "DO", "RE", "PY", "KE", "UY", "KZ", "IR",
            "CO"};
}
