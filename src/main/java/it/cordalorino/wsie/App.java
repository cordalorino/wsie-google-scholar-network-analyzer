package it.cordalorino.wsie;

import it.cordalorino.wsie.analyzer.Analyzer;
import it.cordalorino.wsie.analyzer.Betweenness;
import it.cordalorino.wsie.analyzer.KPPNEG;
import it.cordalorino.wsie.analyzer.LPA;
import it.cordalorino.wsie.analyzer.lucene.LuceneAnalyzer;
import it.cordalorino.wsie.domain.Cluster;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Map;

/**
 * Hello world!
 */
public class App {

    static Logger logger;

    static {
        logger = Logger.getLogger(App.class);
    }

    public static void main(String[] args) {


        DataManager dataDownloader = new DataManager();

        logger.info("Starting scraper execution:");

        dataDownloader.download();

        logger.info("Uploading to db");

        dataDownloader.uploadToDB();

        logger.info("Running analysis:");

        logger.info("Step 1) Looking for communities");
        LPA lpa = new LPA("Author", "COAUTHOR", Analyzer.Direction.IN, 20903, "partition");
        lpa.compute();

        logger.info("Step 2) Looking for Key Players");
        Betweenness betweenness = new Betweenness("Author", "COAUTHOR", "centrality");
        betweenness.compute();

        logger.info("Step 3) Looking for Key Separators");
        KPPNEG kppneg = new KPPNEG("Author", "COAUTHOR", "kppneg");
        kppneg.compute();

        lpa.queryAndDisplay(Analyzer.Order.DESC, 10);
        betweenness.queryAndDisplay(Analyzer.Order.DESC, 10);
        kppneg.queryAndDisplay(Analyzer.Order.DESC, 10);

        logger.info("Step 5) Analyzing words frequencies");
        ArrayList<Cluster> clusters = dataDownloader.getClusters();
        LuceneAnalyzer analyzer = new LuceneAnalyzer(clusters,dataDownloader.getMostCitedId());
        Map<String, Float> tfidf = analyzer.analyzeTFIDF();
        analyzer.getAndPrintFormattedResult(100);

    }

}
