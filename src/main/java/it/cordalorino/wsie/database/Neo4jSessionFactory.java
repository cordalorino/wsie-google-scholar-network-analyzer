package it.cordalorino.wsie.database;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;

public class Neo4jSessionFactory {

    private final static SessionFactory sessionFactory = new SessionFactory("it.cordalorino.wsie.domain");
    private static Neo4jSessionFactory factory = new Neo4jSessionFactory();

    public static Neo4jSessionFactory getInstance() {
        return factory;
    }

    private Neo4jSessionFactory() {
    }

    /**
     * Open a session to the Neo4J database
     * @return Session
     */
    public Session getNeo4jSession() {
        return sessionFactory.openSession();
    }
}