package it.cordalorino.wsie.storage;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import it.cordalorino.wsie.domain.Venue;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Implements some utility methods to handle cache file
 */
public class StorageManager {

    public static final String DEFAULT_ENCODING = "UTF-8";

    public static boolean storeToJson(Object object, String filename){

        String jsonString = new Gson().toJson(object);
        try {
            FileUtils.writeStringToFile(new File(filename),jsonString,DEFAULT_ENCODING);
        } catch (IOException e) {
            return false;
        }
        return true;
    }


    public static <T> T readJsonArrayFile(String filename,Type type){

        try {

            String fileToString = FileUtils.readFileToString(new File(filename), DEFAULT_ENCODING);
            T ts = new Gson().fromJson(fileToString, type);

            return ts;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }



    public static boolean fileExists(String filename) {
        return new File(filename).exists();
    }
}
