package it.cordalorino.wsie.scraper;

import com.google.gson.reflect.TypeToken;
import it.cordalorino.wsie.domain.Venue;
import it.cordalorino.wsie.exception.QuotaExceededException;
import it.cordalorino.wsie.networking.NetworkManager;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * Implements the methods to download and parse the list of all venues in a category
 */
public class VenuesScraper extends Scraper<ArrayList<Venue>> {

    @Override
    public ArrayList<Venue> download(String pageUrl) throws QuotaExceededException {

        Document document = null;
        ArrayList<Venue> venueArrayList = new ArrayList<>();

        try {
            document = NetworkManager.executeRequest(pageUrl);
            Elements titles = document.select("table.gsc_mp_table .gsc_mvt_t");
            Elements hIndexes = document.select("table.gsc_mp_table .gsc_mvt_n");
            for (int i = 1; i < titles.size(); i++) {
                Element tempA = hIndexes.get(2*i).getElementsByTag("a").first();

                String elementLink = tempA.attr("href");
                String elementHIndex = tempA.text();

                String elementHIndexMedian = hIndexes.get(2*i + 1).getElementsByTag("span").text();

                Venue temp = new Venue(titles.get(i).text(), elementLink, Integer.valueOf(elementHIndex),
                        Integer.valueOf(elementHIndexMedian), i-1);
                venueArrayList.add(temp);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return venueArrayList;
    }

    @Override
    public Type getType() {
        return new TypeToken<ArrayList<Venue>>(){}.getType();
    }
}
