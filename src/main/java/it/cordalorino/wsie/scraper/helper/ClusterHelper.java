package it.cordalorino.wsie.scraper.helper;

import it.cordalorino.wsie.domain.Author;
import it.cordalorino.wsie.domain.Cluster;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClusterHelper {
    public static Cluster extractClusterFromSearchResult(Element firstSearchResult) {
        Element titleH3 = firstSearchResult.getElementsByClass("gs_rt").first();
        String title;
        String link;
        Elements gs_ctu = titleH3.getElementsByClass("gs_ctu");
        if (gs_ctu.size() > 0) {
            title = titleH3.text();
            link = null;
        } else {
            Element titleA = titleH3
                    .getElementsByTag("a").first();
            title = titleA.text();
            link = titleA.attr("href");
        }
        Element authorsDiv = firstSearchResult.getElementsByClass("gs_a").first();
//        Cluster cluster = new Cluster();

        Set<Author> authorSet = new HashSet<>();

        Elements authorsAs = authorsDiv.getElementsByTag("a");
        for (int i = 0; i < authorsAs.size(); i++) {
            Element tempAuthor = authorsAs.get(i);
            String name = tempAuthor.text();
            String authorLink = tempAuthor.attr("href");
            String authorID = extractAuthorId(authorLink);
//            cluster.addAuthor(authorID,name);
            authorSet.add(new Author(authorID, name));
        }

        String authorsDivText = authorsDiv.text();
        int year = extractYear(authorsDivText);
        String venueSite = authorsDivText.substring(authorsDivText.lastIndexOf("- ") + 2);
        Elements abstractContainer = firstSearchResult.getElementsByClass("gs_rs");
        String abstractText = null;
        if (abstractContainer.size() > 0) {
            abstractText = abstractContainer.first().text();
        }
        Element linksDiv = firstSearchResult.getElementsByClass("gs_fl").first();

        Element citedByA = linksDiv.getElementsByTag("a").
                first();
        String clusterId = extractClusterId(citedByA);
        int citedBy = Integer.parseInt(citedByA.text().replace("Cited by ", ""));

//        cluster.setAttributes(title,clusterId,citedBy,venueSite,link,abstractText,year);

        Cluster cluster = new Cluster(clusterId, title, citedBy, venueSite, link, abstractText, year, authorSet);

        return cluster;
    }

    public static String extractClusterId(Element citedByA) {
        String citedByLink = citedByA.attr("href");
        String clusterId = extractClusterIdFromLink(citedByLink);
        return clusterId;
    }

    public static String extractClusterIdFromLink(String citedByLink) {
        citedByLink = citedByLink.substring(citedByLink.indexOf("cites=") + "cites=".length());
        citedByLink = citedByLink.substring(0, citedByLink.indexOf("&"));
        return citedByLink;
    }

    public static int extractYear(String authorsDivText) {
        Pattern MY_PATTERN = Pattern.compile("\\d{4}");
        Matcher m = MY_PATTERN.matcher(authorsDivText);
        while (m.find()) {
            String s = m.group(0);
            return Integer.parseInt(s);
        }
        return -1;
    }

    private static String extractAuthorId(String authorLink) {
        String id = authorLink.substring(authorLink.indexOf("user=") + "user=".length());
        id = id.substring(0, id.indexOf("&"));
        return id;
    }

}
