package it.cordalorino.wsie.scraper;

import it.cordalorino.wsie.exception.QuotaExceededException;
import it.cordalorino.wsie.storage.StorageManager;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;

/**
 * Class used to download data from websites.
 * It uses a basic cache mechanism *
 * @param <T> Class of the returning object
 */
public abstract class Scraper<T> {

    static Logger logger;

    static {
        logger = Logger.getLogger(Scraper.class);
    }

    /**
     * Check if cached first, if not download then save
     *
     * @param pageUrl
     * @param cacheFilename
     * @return Parsed object with the page data
     * @throws QuotaExceededException Site don't allow more requests
     */
    public T scrap(String pageUrl, String cacheFilename) throws QuotaExceededException {


        T resultValue;


        if (StorageManager.fileExists(cacheFilename)) {
            logger.info("Reading from disk..." + cacheFilename);

            resultValue = StorageManager.<T>readJsonArrayFile(cacheFilename, getType());
            return resultValue;
        }

        logger.info("Downloading...");
        try {
            resultValue = download(pageUrl);
            while (resultValue == null) {
                resultValue = download(pageUrl);
            }
            StorageManager.storeToJson(resultValue, cacheFilename);
        } catch (QuotaExceededException ex) {
            throw ex;
        }

        return resultValue;
    }

    /**
     * Implements this to define how to download and parse the page
     *
     * @param pageUrl
     * @return Parsed object with the page data
     * @throws QuotaExceededException Site don't allow more requests
     */
    public abstract T download(String pageUrl) throws QuotaExceededException;


    /**
     * Get the type of the object returned from this class download method
     * @return Class
     */
    public abstract Type getType();
}
