package it.cordalorino.wsie.scraper;

import com.google.gson.reflect.TypeToken;
import it.cordalorino.wsie.scraper.helper.ClusterHelper;
import it.cordalorino.wsie.domain.CitationPage;
import it.cordalorino.wsie.domain.Cluster;
import it.cordalorino.wsie.exception.EndOfCitationsReached;
import it.cordalorino.wsie.exception.QuotaExceededException;
import it.cordalorino.wsie.networking.NetworkManager;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;


/**
 * Implements the methods to download and parse the list of all articles that cites one article
 */
public class MostPopularClusterScraper extends Scraper<CitationPage> {

    @Override
    public CitationPage download(String pageUrl) throws QuotaExceededException, EndOfCitationsReached {
        try {
            Document document;
            do {
                document = NetworkManager.executeRequest(pageUrl);
                if (hasCaptcha(document))
                    NetworkManager.renewIp();
            }
            while (hasCaptcha(document));

            String clusterId = ClusterHelper.extractClusterIdFromLink(pageUrl);
            Elements gs_ri = document.getElementsByClass("gs_ri");
            Set<Cluster> clusters = new HashSet<>();
            for (Element e :
                    gs_ri) {
                Cluster cluster = ClusterHelper.extractClusterFromSearchResult(e);
                clusters.add(cluster);
            }

            return new CitationPage(new Cluster(clusterId), clusters);


        } catch (NullPointerException ex) {
            NetworkManager.renewIp();
            return download(pageUrl);
        } catch (HttpStatusException ex) {
            ex.printStackTrace();
            throw new QuotaExceededException();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean hasCaptcha(Document document) {
        return documentCaptcha(document) != null;
    }

    private Element documentCaptcha(Document document) {
        return document.getElementById("gs_captcha_f");
    }

    @Override
    public Type getType() {
        return new TypeToken<CitationPage>() {
        }.getType();
    }
}
