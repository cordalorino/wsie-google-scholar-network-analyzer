package it.cordalorino.wsie.scraper;

import com.google.gson.reflect.TypeToken;
import it.cordalorino.wsie.scraper.helper.ClusterHelper;
import it.cordalorino.wsie.domain.Cluster;
import it.cordalorino.wsie.exception.QuotaExceededException;
import it.cordalorino.wsie.networking.NetworkManager;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * Implements the methods to download and parse
 * the data from all the version of the same article (Clusters in Google Scholar)
 */
public class ClusterScraper extends Scraper<Cluster> {


    @Override
    public Cluster download(String pageUrl) throws QuotaExceededException {

        try {
            Document document = NetworkManager.executeRequest(pageUrl);
            Element firstSearchResult = document.getElementsByClass("gs_ri").first();
            Cluster cluster = ClusterHelper.extractClusterFromSearchResult(firstSearchResult);

            return cluster;
        } catch (NullPointerException ex){
            NetworkManager.renewIp();
            return download(pageUrl);
        }catch (HttpStatusException ex){
            ex.printStackTrace();
            throw new QuotaExceededException();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public Type getType() {
        return new TypeToken<Cluster>(){}.getType();

    }



}
