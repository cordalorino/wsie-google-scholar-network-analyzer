package it.cordalorino.wsie.scraper;

import com.google.gson.reflect.TypeToken;
import it.cordalorino.wsie.constants.Addresses;
import it.cordalorino.wsie.domain.Author;
import it.cordalorino.wsie.domain.Keyword;
import it.cordalorino.wsie.exception.QuotaExceededException;
import it.cordalorino.wsie.networking.NetworkManager;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

/**
 * Implements the methods to download and parse author's information from his/her page
 */
public class AuthorScraper extends Scraper<Author> {
    @Override
    public Author download(String pageUrl) throws QuotaExceededException {

        try {
            Document document = NetworkManager.executeRequest(pageUrl);
            String name = document.getElementById("gsc_prf_in").text();
            Elements divAuthorDescription = document.getElementsByClass("gsc_prf_il");
            Element divKeyword = divAuthorDescription.get(1);

            Set<Keyword> keywords = extractKeywords(divKeyword);
            Set<Author> coAuthors = new HashSet<>();

            Elements coAuthorsContainer = document.getElementsByClass("gsc_rsb_lc");
            if (coAuthorsContainer.size() > 0) {
                String coAuthorsLink = coAuthorsContainer.first().attr("href");

                Document documentCoauthors = NetworkManager.executeRequest(Addresses.SCHOLAR_DOMAIN + coAuthorsLink);

                Elements coauthorsDivs = documentCoauthors.getElementsByClass("gsc_1usr gs_scl");
                for (Element e :
                        coauthorsDivs) {
                    Element nameDiv = e.getElementsByClass("gsc_1usr_name").first();
                    Elements nameA = nameDiv.getElementsByTag("a");
                    String coAuthorName = nameA.text();
                    String coAuthorId = nameA.attr("href");
                    coAuthorId = extractAuthorIdFromLink(coAuthorId);
                    Set<Keyword> coAuthorKeword = extractKeywords(e.getElementsByClass("gsc_1usr_int").first());
                    coAuthors.add(new Author(coAuthorId, coAuthorName, null, coAuthorKeword));
                }
            }

            String authorId = pageUrl.substring(pageUrl.indexOf("user=") + "user=".length());
            return new Author(authorId, name, coAuthors, keywords);


        } catch (NullPointerException ex) {
            NetworkManager.renewIp();
            return download(pageUrl);
        } catch (HttpStatusException ex) {
            ex.printStackTrace();
            throw new QuotaExceededException();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String extractAuthorIdFromLink(String coAuthorId) {
        coAuthorId = coAuthorId.substring(coAuthorId.indexOf("user=") + "user=".length());
        coAuthorId = coAuthorId.substring(0, coAuthorId.indexOf("&"));
        return coAuthorId;
    }

    private Set<Keyword> extractKeywords(Element divKeyword) {
        Elements keywordsA = divKeyword.getElementsByTag("a");
        Set<Keyword> keywords = new HashSet<>();
        for (Element a :
                keywordsA) {
            String label = a.attr("href");
            label = label.substring(label.indexOf("s=label:") + "s=label:".length());
            String keyword = a.text();
            keywords.add(new Keyword(label, keyword));
        }
        return keywords;
    }

    @Override
    public Type getType() {
        return new TypeToken<Author>() {
        }.getType();
    }
}
