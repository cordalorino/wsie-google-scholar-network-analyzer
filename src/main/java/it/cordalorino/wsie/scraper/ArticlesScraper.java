package it.cordalorino.wsie.scraper;

import com.google.gson.reflect.TypeToken;
import it.cordalorino.wsie.domain.Article;
import it.cordalorino.wsie.exception.QuotaExceededException;
import it.cordalorino.wsie.networking.NetworkManager;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Implements the methods to download and parse the list of articles from each venue page.
 */
public class ArticlesScraper extends Scraper<ArrayList<Article>> {

    @Override
    public ArrayList<Article> download(String pageUrl) throws QuotaExceededException{

        ArrayList<Article> articles = new ArrayList<>();

        try {
            Document document = NetworkManager.executeRequest(pageUrl);
            Element table = document.getElementById("gsc_mpat_table");
            Elements titles = table.getElementsByClass("gsc_mpat_ttl");
            Elements citedBy = table.getElementsByClass("gsc_mpat_c");
            Elements years = table.getElementsByClass("gsc_mpat_y");
            for (int i = 1; i < citedBy.size(); i++) {
                Element citedByA = citedBy.get(i).getElementsByTag("a").first();
                Element titleA = titles.get(i-1).getElementsByTag("a").first();

                String citedByLink = citedByA.attr("href");
                String titleLink = titleA.attr("href");
                String title = titleA.text();

                String clusterIdTemp = titleLink.substring(titleLink.indexOf("&cluster=") + "&cluster=".length());

                String clusterId = clusterIdTemp.substring(0, clusterIdTemp.indexOf('&'));

                String year = years.get(i).getElementsByTag("span").first().text();

                Article tempArticle = new Article(title,clusterId,year,citedByLink);
                articles.add(tempArticle);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return articles;
    }

    @Override
    public Type getType() {
        return new TypeToken<ArrayList<Article>>(){}.getType();
    }
}
